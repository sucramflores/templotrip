// ROUTES
templotripApp.config(function ($routeProvider) {
   
    $routeProvider
    
    .when('/', {
        templateUrl: 'home.htm',
        controller: 'homeController'
    })
    
     .when('/ala/:idWard?', {
        templateUrl: 'membros.htm',
        controller: 'membrosController'
    })
    
    .when('/membros/:idMembro', {
        templateUrl: 'membroadd.htm',
        controller: 'membroaddController'
    })
    
    .when('/caravanas/', {
        templateUrl: 'caravanas.htm',
        controller: 'caravanasController'
    })
    
     .when('/caravanas/:idCaravana/', {
        templateUrl: 'caravanasadd.htm',
        controller: 'caravanasaddController'
    })
    
    .when('/autorizacoes/:idAla?/:idMembro?/:idCaravana?/:libera?', {
        templateUrl: 'autorizacoes.htm',
        controller: 'autorizacoesController'
    })
    
    .when('/minhasviagens/', {
        templateUrl: 'minhasviagens.htm',
        controller: 'minhasviagensController'
    })
    
    .when('/listapassageiros/:idCaravana/', {
        templateUrl: 'listapassageiros.htm',
        controller: 'listapassageirosController'
    })
    
});


