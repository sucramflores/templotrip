templotripApp.controller('mainController', ['$scope', '$http', '$resource', '$routeParams', 'templotripService', function($scope, $http, $resource, $routeParams, templotripService) {
      
    $scope.NomeMembro = templotripService.NomeMembro;
    $scope.IDMembro = templotripService.IDMembro;
    $scope.TipoMembro = templotripService.TipoMembro;
    $scope.AlaMembro = templotripService.AlaMembro;
    $scope.IDAlaMembro = templotripService.IDAlaMembro;
   
    var montaMenu = function(){
      if($scope.TipoMembro.trim()==="M"){
//          console.log('Eh membro!');
      }else if($scope.TipoMembro.trim()==='AA'){
          $(".navbar-chamado").text("Admin da Ala");
          $("#menuDivisor").prepend('<li><a class=\'left navbar-item\' href=\'#/ala/'+$scope.IDAlaMembro+'\'>Membros da Ala</a></li>');
          $("#menuDivisor").prepend("<li><a class='navbar-item' href='#/autorizacoes/'>Autorizacoes</a></li>");
      }else if($scope.TipoMembro.trim()==='AE'){
          $(".navbar-chamado").text("Admin da Estaca");
          $("#menuDivisor").prepend("<li><a class='navbar-item' href='#/ala/'>Membros da Estaca</a></li>");
          $("#menuDivisor").prepend("<li><a class='navbar-item' href='#/autorizacoes/'>Autorizacoes</a></li>");
          $("#menuDivisor").prepend("<li><a class='navbar-item' href='#/caravanas/'>Caravanas</a></li>");
      }
    };
    
   
    montaMenu();
    
}]);


templotripApp.controller('homeController', ['$scope', '$http', '$resource', '$routeParams', 'templotripService', function($scope, $http, $resource, $routeParams, templotripService) {
    
        //$scope.data = "15 Julho, 2019";
        //$scope.saida = "01:00am";
        //$scope.retorno = "10:00pm";
        //$scope.local = "Sede da Estaca, Rua Appel 452";
        //$scope.vagas = 53;
        //$scope.obs = "Nao Havera batisterio!";
    
    $scope.NomeMembro = templotripService.NomeMembro;
    $scope.IDMembro = templotripService.IDMembro;
    $scope.TipoMembro = templotripService.TipoMembro;
    $scope.AlaMembro = templotripService.AlaMembro;
    $scope.valorpago ="";
    $scope.ConfirmaInteresse = function(){
        $scope.date = new Date ($("#datapgto").val());
        if($scope.valorpago!==""){
            let dateTemp = $scope.date.getUTCFullYear()+"-"+($scope.date.getUTCMonth() + 1)+"-"+$scope.date.getUTCDate();
            var requ = {
                method: 'POST',
                url: '/api/listapassageiros/add',
                data: {
                    'idCaravana': $('#idCaravana').val(),                    
                    'idMembro': $scope.IDMembro,
                    'ValorPago': $scope.valorpago,
                    'PgtoDate': dateTemp,
                    'LiberadoAla': 0,
                    'tempLiberadoEstaca': 0,
                    'Obs': ''
                }
            }
            $http(requ).then(
                function successCallback(response){
                    Materialize.toast('Sua solicitacao foi enviada com sucesso', 4000,'',
                        function(){
                            window.location.href = '/main.htm'
                    });
                },
                function errorCallback(response){
                    console.log("deu errado!\n"+response);
                });
           }
           else{
                Materialize.toast('Voce precisa informar Valor e Data de pagamento', 4000);
           }
       };
     
    $scope.CancelaInteresse = function(){
        var requ = {
            method: 'DELETE',
            url: '/api/listapassageiros/'+$('#idCaravana').val()+'/'+$scope.IDMembro
        }
        $http(requ).then(
            function successCallback(response){
                Materialize.toast('Sua solicitacao foi enviada com sucesso', 4000,'',
                    function(){
                        window.location.href = '/main.htm'
                });
            },
            function errorCallback(response){
                console.log("deu errado!\n"+response);
            });

   };    
        
        
        
    
    
    
    $scope.convertDate = function(dateParam){
        return convertDate(dateParam); //in script.js
    }
    
    
    $http({
        method: 'GET',
        url: "/api/proximacaravanas/"+$scope.IDMembro
    }).then(function successCallback(response) {
        $scope.proximacaravana = response.data;
    }, function errorCallback(response) {
        console.log('erro '+response);
    });
 
    
    //$scope.days = $routeParams.days || '2';
    
    
}]);



templotripApp.controller('membrosController', ['$scope', '$http', '$resource', '$routeParams', 'templotripService', function($scope, $http, $resource, $routeParams, templotripService) {
      
    $scope.NomeMembro = templotripService.NomeMembro;
    $scope.IDMembro = templotripService.IDMembro;
    $scope.TipoMembro = templotripService.TipoMembro;
    $scope.AlaMembro = templotripService.AlaMembro;
    
    let urlTemp = "/api/membros/0"  // 0 means NO id for membros ()
    if($routeParams['idWard']){
        urlTemp +='/'+$routeParams['idWard'];
    }
    
    $http({
        method: 'GET',
        url: urlTemp
    }).then(function successCallback(response) {
        $scope.Ala = response.data;
    }, function errorCallback(response) {
        console.log('erro '+response);
    });
    
    
}]);






templotripApp.controller('membroaddController', ['$scope', '$http', '$resource', '$routeParams', 'templotripService', function($scope, $http, $resource, $routeParams, templotripService) {
      
    $scope.NomeMembro = templotripService.NomeMembro;
    $scope.IDMembro = templotripService.IDMembro;
    $scope.TipoMembro = templotripService.TipoMembro;
    $scope.AlaMembro = templotripService.AlaMembro;
    
    let urlTemp = "/api/membros/"  // 0 means NO id for membros ()
    if($routeParams['idMembro']==="add"){
        
    }else if(!isNaN($routeParams['idMembro'])){ // Se envia ID entao eh EDICAO 
        $("button[type=\"submit\"]").hide(); 
        urlTemp +=$routeParams['idMembro'];
        $http({
            method: 'GET',
            url: urlTemp
        }).success(function(data, status, header, config) {
            $scope.objAla = data[0]['Ala'];  // Nao confundia objAla com AlaMembro
            $scope.objAlaID = data[0]['idAla'];  // Nao confundia objAla com AlaMembro
            $scope.objNome = data[0]['Nome'];
            $scope.objEmail = data[0]['Email'];
            $scope.objFone = data[0]['Fone'];
            $scope.objRG = data[0]['RG'];
            $scope.objCN = data[0]['CN'];
            $scope.objChamado = data[0]['Chamado'];
            $scope.objStatus = data[0]['StatusConta'];
            $('label').addClass('active'); // prevents the label overlap the content of the input field
            $('#idala').find('option[value="'+$scope.objAlaID+'"]').prop('selected', true); // Data binding nao funcionou direito.
            $('#idala').material_select(); // nao faz o data biniding sem um "refresh" no SELECT
            
        }, function errorCallback(response) {
            console.log('erro '+response);
        });
    }
    
   
    // Ajax Call to "Insert" new checklist
    $scope.salvaMembro = function(){
//        console.log('alaid: '+$scope.objAlaID);
//        console.log('objNome: '+$scope.objNome);
//        console.log('objEmail: '+$scope.objEmail);
//        console.log('objFone: '+$scope.objFone);
//        console.log('objRG: '+$scope.objRG);
//        console.log('objCN: '+$scope.objCN);
//        console.log('objChamado: '+$scope.objChamado);
//        console.log('objStatus: '+$scope.objStatus);

        var requ = {
            method: 'POST',
            url: "/api/membros/add",
            data: {
                'idAla':$scope.objAlaID,                    
                'Nome':$scope.objNome,
                'Email':$scope.objEmail,
                'Fone':$scope.objFone,
                'RG':$scope.objRG, 
                'CN':$scope.objCN,
                'StatusConta':$scope.objStatus,
                'Chamado':$scope.objChamado
            }
        }

        $http(requ).then(
            function successCallback(response){
                // console.log("deu certo!");
                Materialize.toast('Registro Enviado com Sucesso!', 4000);
                window.location.href = 'main.htm#/ala/' ;
            },
            function errorCallback(response){
                console.log("deu errado!\n"+response);
        });
    }
    
    
}]);



templotripApp.controller('caravanasController', ['$scope', '$http', '$resource', '$routeParams', 'templotripService', function($scope, $http, $resource, $routeParams, templotripService) {
    $scope.NomeMembro = templotripService.NomeMembro;
    $scope.IDMembro = templotripService.IDMembro;
    $scope.TipoMembro = templotripService.TipoMembro;
    $scope.AlaMembro = templotripService.AlaMembro;
    
    $scope.convertDate = function(dateParam){
        return convertDate(dateParam); //in script.js
    }
    
    $http({
        method: 'GET',
        url: "/api/caravanas/"
    }).then(function successCallback(response) {
        $scope.Caravanas = response.data;
    }, function errorCallback(response) {
        console.log('erro '+response);
    });
    
    
}]);



templotripApp.controller('caravanasaddController', ['$scope', '$http', '$resource', '$routeParams', 'templotripService', function($scope, $http, $resource, $routeParams, templotripService) {
      
    $scope.NomeMembro = templotripService.NomeMembro;
    $scope.IDMembro = templotripService.IDMembro;
    $scope.TipoMembro = templotripService.TipoMembro;
    $scope.AlaMembro = templotripService.AlaMembro;
    
    let urlTemp = "/api/caravanas/"  // 0 means NO id for membros ()
    if($routeParams['idCaravana']==="add"){
        
    }else if(!isNaN($routeParams['idCaravana'])){ // Se envia ID entao eh EDICAO 
        $("button[type=\"submit\"]").hide(); 
        $("#btnExcluiRegistro").removeClass("hide"); 
        urlTemp +=$routeParams['idCaravana'];
        $http({
            method: 'GET',
            url: urlTemp
        }).success(function(data, status, header, config) {
            $scope.objIDCarvana = data[0]['idCaravana'] ;  
            $scope.objDataSaida = convertDate(data[0]['Data']);  
            $scope.objHoraSaida = data[0]['HoraSaida'];  
            $scope.objHoraRetorno = data[0]['HoraRetorno'];
            $scope.objLocalSaida = data[0]['LocalSaida'];
            $scope.objNumeroOnibus = data[0]['QtdOnibus'];
            $scope.objTotalLugares = data[0]['TotalLugares'];
            $scope.objValorPassagem = data[0]['ValorPassagem'];
            $scope.objObs = data[0]['Obs'];
            $('label').addClass('active'); // prevents the label overlap the content of the input field
        }, function errorCallback(response) {
            console.log('erro '+response);
        });
    }
    
   
    // Ajax Call to "Insert" new caravana
    $scope.salvaCaravana = function(){
//        console.log('objDataSaida: '+$scope.objDataSaida);
//        console.log('objHoraSaida: '+$scope.objHoraSaida);
//        console.log('objHoraRetorno: '+$scope.objHoraRetorno);
//        console.log('objLocalSaida: '+$scope.objLocalSaida);
//        console.log('objNumeroOnibus: '+$scope.objNumeroOnibus);
//        console.log('objTotalLugares: '+$scope.objTotalLugares);
//        console.log('objValorPassagem: '+$scope.objValorPassagem);
//        console.log('objObs: '+$scope.objObs);

        $scope.date = new Date ($('#cldata').val());
        let dateTemp = $scope.date.getUTCFullYear()+"-"+($scope.date.getUTCMonth() + 1)+"-"+$scope.date.getUTCDate();

         if($scope.date=="Invalid Date"){
            Materialize.toast('Informe a data da caravana', 4000);
            return;
        }
        
        var requ = {
            method: 'POST',
            url: "/api/caravanas/add",
            data: {
                'idResponsavel':0,    
                'CreatedBy':0,    
                'Data':dateTemp,    
                'LocalSaida':$scope.objLocalSaida,
                'HoraRetorno':$scope.objHoraRetorno,
                'HoraSaida':$scope.objHoraSaida,
                'QtdOnibus':$scope.objNumeroOnibus, 
                'TotalLugares':$scope.objTotalLugares,
                'Obs':$scope.objObs,
                'ValorPassagem':$scope.objValorPassagem
            }
        }

        $http(requ).then(
            function successCallback(response){
                // console.log("deu certo!");
                Materialize.toast('Registro Enviado com Sucesso!', 4000);
                window.location.href = 'main.htm#/caravanas/' ;
            },
            function errorCallback(response){
                console.log("deu errado!\n"+response);
        });
    }
    
    
    $scope.excluiCaravana = function(){
    
        var requ = {
            method: 'DELETE',
            url: "/api/caravanas/"+$scope.objIDCarvana
        }

        $http(requ).then(
            function successCallback(response){
                // console.log("deu certo!");
                Materialize.toast('Registro Excluido!', 4000);
                window.location.href = 'main.htm#/caravanas/' ;
            },
            function errorCallback(response){
                console.log("deu errado!\n "+response);
        });
    }
    
}]);



templotripApp.controller('autorizacoesController', ['$scope', '$http', '$resource', '$routeParams', 'templotripService', function($scope, $http, $resource, $routeParams, templotripService) {
    $scope.NomeMembro = templotripService.NomeMembro;
    $scope.IDMembro = templotripService.IDMembro;
    $scope.TipoMembro = templotripService.TipoMembro;
    $scope.AlaMembro = templotripService.AlaMembro;
    $scope.IDAlaMembro = templotripService.IDAlaMembro;
    
    $scope.convertDate = function(dateParam,flag){
        return convertDate(dateParam,flag); //in script.js
    }
    
   
   
    
    if($routeParams['idCaravana'] && $routeParams['idMembro'] && $routeParams['libera']){ // Autoriza ou desautoriza de acordo com o parametro
        let apiURL = "/api/autoriza/"+$routeParams['idCaravana']+"/"+$routeParams['idMembro']+"/"+$scope.TipoMembro+"/"+$routeParams['libera']

        var requ = {
            method: 'GET',
            url: apiURL
        }
        $http(requ).then(
            function successCallback(response){
                // console.log("deu certo!");
                window.location.href = 'main.htm#/autorizacoes/' ;
            },
            function errorCallback(response){
                console.log("deu errado!\n"+response);
        });

        
    }
    
    let urlTemp = "/api/listapassageiros/0/";
    if($scope.TipoMembro=='AA'){
        urlTemp+= $scope.IDAlaMembro  
    }
    
    $http({
        method: 'GET',
        url: urlTemp
    }).then(function successCallback(response) {
        $scope.Autorizacoes = response.data;
        if($scope.TipoMembro==='AE'){
            $(".colAla").attr("href","#/autorizacoes/")
        }else{
            $(".colEstaca").attr("href","#/autorizacoes/")
        }
    }, function errorCallback(response) {
        console.log('erro '+response);
    });
    
   
    
    
}]);


templotripApp.controller('minhasviagensController', ['$scope', '$http', '$resource', '$routeParams', 'templotripService', function($scope, $http, $resource, $routeParams, templotripService) {
      
    $scope.NomeMembro = templotripService.NomeMembro;
    $scope.IDMembro = templotripService.IDMembro;
    $scope.TipoMembro = templotripService.TipoMembro;
    $scope.AlaMembro = templotripService.AlaMembro;
    
    $scope.convertDate = function(dateParam){
        return convertDate(dateParam); //in script.js
    }
    
    let urlTemp = "/api/listapassageiros/0/0/"+$scope.IDMembro  
    
    $http({
        method: 'GET',
        url: urlTemp
    }).then(function successCallback(response) {
        $scope.minhasviagens = response.data;
    }, function errorCallback(response) {
        console.log('erro '+response);
    });
    
    
}]);


templotripApp.controller('listapassageirosController', ['$scope', '$http', '$resource', '$routeParams', 'templotripService', function($scope, $http, $resource, $routeParams, templotripService) {
      
    $scope.NomeMembro = templotripService.NomeMembro;
    $scope.IDMembro = templotripService.IDMembro;
    $scope.TipoMembro = templotripService.TipoMembro;
    $scope.AlaMembro = templotripService.AlaMembro;
    
    $scope.convertDate = function(dateParam){
        return convertDate(dateParam); //in script.js
    }
    
    $scope.excluiPassageiro = function(idcaravana,idmembro){
        var requ = {
            method: 'DELETE',
            url: "/api/listapassageiros/"+idcaravana+"/"+idmembro
        }

        $http(requ).then(
            function successCallback(response){
                // console.log("deu certo!");
                Materialize.toast('Registro Excluido!', 4000);
                window.location.href = 'main.htm#/caravanas/' ;
            },
            function errorCallback(response){
                console.log("deu errado!\n "+response);
        });
    }
    
    if($routeParams['idCaravana']){ 
        let urlTemp = "/api/listapassageiros/"+$routeParams['idCaravana']
        $http({
            method: 'GET',
            url: urlTemp
        }).then(function successCallback(response) {
            $scope.listapassageiros = response.data;
        }, function errorCallback(response) {
            console.log('erro '+response);
        });
    }
    
}]);