var express = require('express')
    app = express()
    fs = require("fs")
    sql = require('mssql')
    bodyParser = require('body-parser')

var dbConnection = {
    user: 'admin',
    password: '123456',
    server: 'SUCRAM\\SQLEXPRESS',
    database: 'templetrip'
};

app.use(express.static(__dirname + '/Static'));
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(bodyParser.json());

//  To filter by branchid ex) http://localhost:3000/api/list?table=fillrates&branchid=3
//  app.use(bodyParser.json()); // support json encoded bodies
//  http://127.0.0.1:8081/api?table=membros
//  http://127.0.0.1:8081/api/default?table=membros&id=1

//app.get("/api/membros/", function(req, res) {
//    sql.connect(dbConnection).then(function (connection) {
//        parametroID = 1;
//        let stringSQL = 'select * from membros where id=1';
//        new sql.Request(connection).query(stringSQL).then(function(recordset) {
//            console.log(recordset);
////            res.send(recordset);
//        }).catch(function(err) {
//            res.send(err);
//        });
//    }).catch(function(err) {
//        console.log(err);
//    });
//
//})





//--------------------------------------------------------------------
//                           GET endPoints
//--------------------------------------------------------------------



//app.get('/listUsers', function (req, res) {
//   fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
//       console.log( data );
//       res.end( data );
//   });
//})


app.get('/api/login/:id?', function(req, res) {
    // console.log(req.params.id);
    sql.connect(dbConnection).then(function (connection) {
        //tableName = req.query.table;
        let querySQL = "select * from logins";
        if (req.params.id) {
            querySQL += ' where IdMembro = ' + req.params.id;
        }
        console.log(querySQL);   
        new sql.Request(connection).query(querySQL).then(function(recordset) {
            res.send(recordset);
        }).catch(function(err) {
            res.send(err);
        });
    }).catch(function(err) {
        console.log("straight up errors :("+err);
    });
})





// if id is null or 0 then check idWard, else show all records
app.get('/api/membros/:id?/:idWard?', function(req, res) {
    // console.log(req.params.id);
    sql.connect(dbConnection).then(function (connection) {
        //tableName = req.query.table;
        let querySQL = "select * from vmembros";
        if (req.params.id && req.params.id>0) {
            querySQL += ' where IdMembro = ' + req.params.id;
        }else if (req.params.idWard){
            querySQL += ' where IdAla = ' + req.params.idWard;
        }
        querySQL += ' order by ala,nome'
        // console.log(querySQL);   
        new sql.Request(connection).query(querySQL).then(function(recordset) {
            res.send(recordset);
        }).catch(function(err) { 
            res.send(err);
        });
    }).catch(function(err) {
        console.log("straight up errors :( "+err);
    });
});





// choose between idcaravana or data (not both); if date is picked then pass idcaravana as 0
app.get('/api/caravanas/:id?/:Data?', function(req, res) {
    // console.log(req.params.id);
    sql.connect(dbConnection).then(function (connection) {
        //tableName = req.query.table;
        let querySQL = "select * from vcaravanas";
        if (req.params.id && req.params.id>0) {
            querySQL += ' where IdCaravana = ' + req.params.id;
        }else if (req.params.Data){
            querySQL += ' where Data = \'' + req.params.Data+'\'';
        }
        // console.log(querySQL);   
        new sql.Request(connection).query(querySQL).then(function(recordset) {
            res.send(recordset);
        }).catch(function(err) {
            res.send(err);
        });
    }).catch(function(err) {
        console.log("straight up errors :( "+err);
    });
});



// Show next CARAVANA
app.get('/api/proximacaravanas/:idMembro', function(req, res) {
    console.log(req.params.id);
    sql.connect(dbConnection).then(function (connection) {
        //tableName = req.query.table;
        //let querySQL = "select * from vcaravanasCalc where data >= GETDATE()";
        let querySQL = "select *,(select 'true' from ListaPassageiros as l where l.idCaravana=vcaravanasCalc.idCaravana and l.idMembro="+req.params.idMembro+") as naLista, (select 'true' from ListaPassageiros as l where l.idCaravana=vcaravanasCalc.idCaravana and l.idMembro="+req.params.idMembro+" and l.LiberadoAla>0) as liberadoAla, (select 'true' from ListaPassageiros as l where l.idCaravana=vcaravanasCalc.idCaravana and l.idMembro="+req.params.idMembro+" and l.LiberadoEstaca>0) as liberadoEstaca from vCaravanasCalc where data >= GETDATE()";
        new sql.Request(connection).query(querySQL).then(function(recordset) {
            res.send(recordset);
        }).catch(function(err) {
            res.send(err);
        });
    }).catch(function(err) {
        console.log("straight up errors :( "+err);
    });
});


// choose between idcaravana, data or idmembros (pick one); if date is picked then pass idcaravana and data as 0
app.get('/api/listapassageiros/:id?/:idAla?/:idmembro?', function(req, res) {
//    console.log(req.params)
    sql.connect(dbConnection).then(function (connection) {
        let querySQL = "select * from vListaPassageiros";
        if (req.params.id && req.params.id>0) {
            querySQL += ' where IdCaravana = ' + req.params.id;
        }else if (req.params.idAla && req.params.idAla!=0){
            querySQL += ' where idAla = ' + req.params.idAla;
        }else if (req.params.idmembro && req.params.idmembro>0){
            querySQL += ' where idMembro = ' + req.params.idmembro;
        }
        // console.log(querySQL);   
        new sql.Request(connection).query(querySQL).then(function(recordset) {
            res.send(recordset);
        }).catch(function(err) {
            res.send(err);
        });
    }).catch(function(err) {
        console.log("straight up errors :( "+err);
    });
});












//----------------------------------------------------------
//                      POST endPoints
//----------------------------------------------------------


app.post('/api/autent/', (req, res) => {    // route to login (authenticate)
    console.log(req.body);
    if(isEmpty(req.body)){
        res.send("Opz! Voce precisa informar  algum login!");   
    }else{
        sql.connect(dbConnection).then(function (connection) {
            let querySQL = "select * from vlogins";
            if (!isEmpty(req.body.username)) {
                querySQL += " where login = \'" + req.body.username+"\'";
            }
            console.log(querySQL);   
            new sql.Request(connection).query(querySQL).then(function(recordset) {
                res.send(recordset);
            }).catch(function(err) {
                res.send(err);
            });
        }).catch(function(err) {
                console.log("straight up errors :( "+err);
            });    
        }
});




app.post('/api/login/add', (req, res) => {
       if(isEmpty(req.body)){
        res.send("Opz! Nao ha dados para incluir.");   
    }else{
        if (req.body.idMembro.length!==0 && req.body.Login.length!==0 && req.body.Pwd.length!==0 && req.body.Tipo.length!==0) {
            sql.connect(dbConnection).then(function (connection) {
                let querySQL = "insert into Logins(idMembro,Login,Pwd,Tipo) values("+
                    req.body.idMembro + ",'"+req.body.Login+"','"+req.body.Pwd+"','"+req.body.Tipo+"')";
                console.log(querySQL);   
                new sql.Request(connection).query(querySQL).then(function(recordset) {
                    res.send(true)
                }).catch(function(err) {
                    res.send(err);
                });
            }).catch(function(err) {
                    console.log("Opz! :("+err);
                });    
            }
        else{
            res.send("Nome e idAla precisam ser preenchidos!") // replace by FALSE (to be returned to frontend)
        }
    }
});



app.post('/api/membros/add', (req, res) => {
    console.log(req.body);
    if(isEmpty(req.body)){
        res.send("Opz! Nao ha dados para incluir.");   
    }else{
        if (req.body.Nome.length!==0 && req.body.idAla.length!==0) {
            sql.connect(dbConnection).then(function (connection) {
                let querySQL = "insert into Membros(idAla,Nome,Email,Fone,RG,CN,StatusConta,Chamado) values("+
                    req.body.idAla + ",'"+req.body.Nome+"','"+req.body.Email+"','"+req.body.Fone+"','"+req.body.RG+"','"+req.body.CN+"','"+
                    req.body.StatusConta+"','"+req.body.Chamado+"')";
                console.log(querySQL);   
                new sql.Request(connection).query(querySQL).then(function(recordset) {
                    res.send(true)
                }).catch(function(err) {
                    res.send(err);
                });
            }).catch(function(err) {
                    console.log("Opz! :("+err);
                });    
            }
        else{
            res.send("Nome e idAla precisam ser preenchidos!") // replace by FALSE (to be returned to frontend)
        }
    }
});



app.post('/api/caravanas/add', (req, res) => {
    console.log(req.body);
    if(isEmpty(req.body)){
        res.send("Opz! Nao ha dados para incluir.");   
    }else{
        if (req.body.Data.length!==0 && req.body.CreatedBy.length!==0) {
            sql.connect(dbConnection).then(function (connection) {
                let querySQL = "insert into Caravanas(idResponsavel,CreatedBy,Data,LocalSaida,HoraRetorno,HoraSaida,QtdOnibus,TotalLugares,Obs,ValorPassagem) values("+
                    req.body.idResponsavel + ","+req.body.CreatedBy+",'"+req.body.Data+"','"+req.body.LocalSaida+"','"+req.body.HoraRetorno+"','"+req.body.HoraSaida+"',"+req.body.QtdOnibus+","+
                    req.body.TotalLugares+",'"+req.body.Obs+"',"+req.body.ValorPassagem+")";
                console.log(querySQL);   
                new sql.Request(connection).query(querySQL).then(function(recordset) {
                    res.send(true);
                }).catch(function(err) {
                    res.send(err);
                });
            }).catch(function(err) {
                    console.log("Opz! :("+err);
                });    
            }
        else{
            res.send("Data e Nome do responsavel por essa inclusao nao foram informados!") // replace by FALSE (to be returned to frontend)
        }
    }
});



app.post('/api/listapassageiros/add', (req, res) => {
    //console.log(req.body)
    if(isEmpty(req.body)){
        res.send("Opz! Nao ha dados para incluir."); 
        console.log("Opz! Nao ha dados para incluir."); 
    }else{
        let tempValorPago = req.body.ValorPago || 0;
        let tempLiberadoEstaca = req.body.tempLiberadoEstaca || 0;
        let tempLiberadoAla = req.body.tempLiberadoAla || 0;
        if (req.body.idCaravana.length!==0 && req.body.idMembro.length!==0 && req.body.ValorPago.length!==0) {
            sql.connect(dbConnection).then(function (connection) {
                let querySQL = "insert into ListaPassageiros(idCaravana,idMembro,ValorPago,PgtoDate,LiberadoAla,LiberadoEstaca,Obs) values("+
                    req.body.idCaravana + "," + req.body.idMembro + "," + tempValorPago + ",'" + req.body.PgtoDate + "'," + tempLiberadoAla + "," + tempLiberadoEstaca + ",'" + req.body.Obs + "')";
                console.log(querySQL);   
                new sql.Request(connection).query(querySQL).then(function(recordset) {
                    res.send(true)
                }).catch(function(err) {
                    res.send(err);
                });
            }).catch(function(err) {
                    console.log("Opz! :("+err);
                });    
            }
        else{
            res.send("Data e Nome do responsavel por essa inclusao nao foram informados!") // replace by FALSE (to be returned to frontend)
        }
    }
});







//  ----------------------------------------------------------------
//                          PUT Endpoints
//  ----------------------------------------------------------------
    


app.put('/api/login/', (req, res) => {
       if(isEmpty(req.body)){
        res.send("Opz! Nao ha dados para alterar.");   
    }else{
        if (req.body.idMembro.length!==0 && req.body.Login.length!==0 && req.body.Pwd.length!==0 && req.body.Tipo.length!==0) {
            sql.connect(dbConnection).then(function (connection) {
                let querySQL = "update Logins " +
                    "set Login = '"+req.body.Login+"',"+
                    "Pwd = '"+req.body.Pwd+"',"+
                    "Tipo = '" + req.body.Tipo + "' where IdMembro = "+req.body.idMembro;
                console.log(querySQL);   
                new sql.Request(connection).query(querySQL).then(function(recordset) {
                    res.send(true)
                }).catch(function(err) {
                    res.send(err);
                });
            }).catch(function(err) {
                    console.log("Opz! :( "+err);
                });    
            }
        else{
            res.send("Todos os campos precisam ser preenchidos!") // replace by FALSE (to be returned to frontend)
        }
    }
});


app.put('/api/membros/:id', (req, res) => {
    // console.log(req.body.Nome.length);
    if(isEmpty(req.body)){
        res.send("Opz! Nao ha dados para incluir.");   
    }else{
        if (req.params.id) {
            sql.connect(dbConnection).then(function (connection) {
                let querySQL = "update Membros " +
                    "set idAla = "+req.body.idAla+","+
                    "Nome = '"+req.body.Nome+"',"+
                    "Email = '"+req.body.Email+"',"+
                    "Fone = '"+req.body.Fone+"',"+
                    "RG = '"+req.body.RG+"',"+
                    "CN = '"+req.body.CN+"',"+
                    "StatusConta = '"+req.body.StatusConta+"',"+
                    "Chamado = '"+req.body.Chamado+"' where IdMembro = "+req.params.id;
                console.log(querySQL);   
                new sql.Request(connection).query(querySQL).then(function(recordset) {
                    res.send(true)
                }).catch(function(err) {
                    res.send(err);
                });
            }).catch(function(err) {
                    console.log("Opz! :("+err);
                });    
            }
        else{
            res.send("O Id do membro nao foi informado.") // replace by FALSE (to be returned to frontend)
        }
    }
});




app.put('/api/caravana/:id/', (req, res) => {
    // console.log(req.body.Nome.length);
    if(isEmpty(req.body)){
        res.send("Opz! Nao ha dados para alterar.");   
    }else{
        if (req.params.id) {
            sql.connect(dbConnection).then(function (connection) {
                let querySQL = "update Caravanas " +
                    "set idResponsavel = "+req.body.idResponsavel+","+
                    "CreatedBy = '"+req.body.CreatedBy+"',"+
                    "Data = '"+req.body.Data+"',"+
                    "LocalSaida = '"+req.body.LocalSaida+"',"+
                    "HoraRetorno = '"+req.body.HoraRetorno+"',"+
                    "HoraSaida = '"+req.body.HoraSaida+"',"+
                    "QtdOnibus = '"+req.body.QtdOnibus+"',"+
                    "TotalLugares = '"+req.body.TotalLugares+"',"+
                    "Obs = '"+req.body.Obs+"',"+
                    "ValorPassagem = "+req.body.ValorPassagem+" where IdCaravana = "+req.params.id;
                console.log(querySQL);   
                new sql.Request(connection).query(querySQL).then(function(recordset) {
                    res.send(true)
                }).catch(function(err) {
                    res.send(err);
                });
            }).catch(function(err) {
                    console.log("Opz! :("+err);
                });    
            }
        else{
            res.send("O Id da caravana nao foi informado.") // replace by FALSE (to be returned to frontend)
        }
    }
});


app.put('/api/listapassageiros/:id/:idMembro', (req, res) => {
    console.log(req.body);
    if(isEmpty(req.body)){
        res.send("Opz! Nao ha dados para alterar.");   
    }else{
        if (req.params.id && req.params.idMembro) {
            sql.connect(dbConnection).then(function (connection) {
                let querySQL = "update ListaPassageiros " +
                    "set ValorPago = "+req.body.idResponsavel+","+
                    "PgtoDate = '"+req.body.CreatedBy+"',"+
                    "LiberadoAla = '"+req.body.Data+"',"+
                    "LiberadoEstaca = '"+req.body.LocalSaida+"',"+
                    "Obs = '"+req.body.Obs+"' where IdCaravana = "+req.params.id+ " and idMembro = "+req.params.idMembro;
                console.log(querySQL);   
                new sql.Request(connection).query(querySQL).then(function(recordset) {
                    res.send(true)
                }).catch(function(err) {
                    res.send(err);
                });
            }).catch(function(err) {
                    console.log("Opz! :("+err);
                });    
            }
        else{
            res.send("O Id da caravana ou do membro nao foi informado.") // replace by FALSE (to be returned to frontend)
        }
    }
});



app.get('/api/autoriza/:id/:idMembro/:quem/:oque', (req, res) => {
    if(isEmpty(req.params)){
        console.log("Opz! Nao ha dados para alterar.");   
    }else{
        if (req.params.id && req.params.idMembro && req.params.quem) {
            sql.connect(dbConnection).then(function (connection) {
                let nomecoluna = 'LiberadoAla';
                if(req.params.quem==='AE'){ //admin estaca
                    nomecoluna = 'LiberadoEstaca';
                    console.log("entroua Nodeeee - "+req.params.quem);
                }
                let querySQL = "update ListaPassageiros " +
                    "set "+nomecoluna+" ="+req.params.oque+" where IdCaravana = "+req.params.id+ " and idMembro = "+req.params.idMembro;
                console.log(querySQL);   
                new sql.Request(connection).query(querySQL).then(function(recordset) {
                    res.send(true)
                }).catch(function(err) {
                    res.send(err);
                });
            }).catch(function(err) {
                    console.log("Opz! :( "+err);
                });    
            }
        else{
            res.send("O Id da caravana ou do membro nao foi informado.") // replace by FALSE (to be returned to frontend)
        }
    }
});

//app.post('/addUser', function (req, res) {
//   // First read existing users.
//   fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
//       data = JSON.parse( data );
//       data["user4"] = user["user4"];
//       console.log( data );
//       res.end( JSON.stringify(data));
//   });
//})






// ----------------------------------------------------------------
//                      DELETE endPoints
// ----------------------------------------------------------------


app.delete('/api/membros/:id', (req, res) => {
    // console.log(req.body.Nome.length);
    if (req.params.id) {
        sql.connect(dbConnection).then(function (connection) {
            let querySQL = "delete from Membros where IdMembro = "+req.params.id;
            new sql.Request(connection).query(querySQL).then(function(recordset) {
                res.send(true)
            }).catch(function(err) {
                res.send(err);
            });
        }).catch(function(err) {
                console.log("Opz! :( "+err);
            });    
        }
    else{
        res.send("O Id do membro nao foi informado.") // replace by FALSE (to be returned to frontend)
    }
    
});



app.delete('/api/caravanas/:id', (req, res) => {
    console.log(req.params);
    if (req.params.id) {
        sql.connect(dbConnection).then(function (connection) {
            let querySQL = "delete from Caravanas where idCaravana = "+req.params.id;
            console.log(querySQL);
            new sql.Request(connection).query(querySQL).then(function(recordset) {
                res.send(true)
            }).catch(function(err) {
                res.send(err);
            });
        }).catch(function(err) {
                console.log("Opz! :( "+err);
            });    
        }
    else{
        res.send("O Id da Caravana nao foi informado.") // replace by FALSE (to be returned to frontend)
    }
    
});




app.delete('/api/listapassageiros/:id/:idMembro', (req, res) => {
    // console.log(req.body.Nome.length);
    if (req.params.id) {
        sql.connect(dbConnection).then(function (connection) {
            let querySQL = "delete from ListaPassageiros where idCaravana = "+req.params.id+" and IdMembro="+req.params.idMembro;
            new sql.Request(connection).query(querySQL).then(function(recordset) {
                res.send(true)
            }).catch(function(err) {
                res.send(err);
            });
        }).catch(function(err) {
                console.log("Opz! :( "+err);
            });    
        }
    else{
        res.send("O idMembro ou idCaravna nao foi informado.") // replace by FALSE (to be returned to frontend)
    }
    
});




 


//------------------------------------------------------------------------
//                              Functions
//------------------------------------------------------------------------

function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}



//------------------------------------------------------------------------
//                              MAIN Thread
//------------------------------------------------------------------------

app.get('/', function (req, res) {
    res.sendFile( __dirname + "/" + "login.htm" );
})

//app.get('/main.htm', function (req, res) {
//    res.sendFile( __dirname + "/" + "main.htm" );
//})

//app.get('/membroadd.htm', function (req, res) {
//    res.sendFile( __dirname + "/" + "membroadd.htm" );
//})


//app.get('/pages/:fpath', function (req, res) {
//    res.sendFile( __dirname + "/Pages/" + req.params.fpath);
//})


app.get('*', function (req, res) {
    res.sendFile( __dirname + "/" + req.url)
})

var server = app.listen(80, function () {

  var host = server.address().address
  var port = server.address().port
  console.log("TempleTrip app listening at port %s", port)

});
