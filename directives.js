// DIRECTIVES
templotripApp.directive("proximaCaravana", function() {
   return {
       restrict: 'EA',
       templateUrl: 'proximaCaravana.htm',
       scope: {
           proximaCaravanaObj: "=",
           nomeDoMembro: "@",
           idMembro: "@",
           converteData: "&",
       }
   }
});

