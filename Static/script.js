function convertDate(dateParam,flag){ //get this format = 2017-01-30T00:00:00.000Z
    // if flag eh false, entao nao retorna o dia da semana
    var monthNames = [
        "Janeiro", "Fevereiro", "Marco",
        "Abril", "Maio", "Junho", "Julho",
        "Agosto", "Setembro", "Outubro",
        "Novembro", "Dezembro"
    ];

    var dataAux = new Date(dateParam);
    var day = dataAux.getDate();
    var monthIndex = dataAux.getMonth();
    var year = dataAux.getFullYear();
    var dayOfWeek = -1;
    
    switch(dataAux.getDay()+1) {
    case 1:
        dayOfWeek = "Segunda-feira, ";
        break;
    case 2:
        dayOfWeek = "Terca-feira, ";
        break;
    case 3:
        dayOfWeek = "Quarta-feira, ";
        break;            
    case 4:
        dayOfWeek = "Quinta-feira, ";
        break;            
    case 5:
        dayOfWeek = "Sexta-feira, ";
        break;            
    case 6:
        dayOfWeek = "Sabado, ";
        break;            
    case 7:
        dayOfWeek = "Domingo, ";
        break;                        
    default:
        dayOfWeek = ""
}
    if(flag===0){ //exclui o dia
        dayOfWeek="";
    }
    
//    return day + ' ' + monthNames[monthIndex] + ' ' + year;
    return dayOfWeek+' '+(day+1)+' '+monthNames[monthIndex] + ', ' + year;
}


function CallFormAutentica(){
    var aux = document.getElementById("user").value;
    var loginData = {username:aux};
    $.post("/api/autent", loginData,function(data){
                if(data[0]){
                    window.location.href = 'main.htm';
                    sessionStorage.setItem('NomeMembro',data[0].Nome);
                    sessionStorage.setItem('IDMembro',data[0].idMembro);
                    sessionStorage.setItem('TipoMembro',data[0].Tipo );
                    sessionStorage.setItem('AlaMembro', data[0].AlaMembro);
                    sessionStorage.setItem('IDAlaMembro', data[0].idAla);
                }
                else{
                    $("#loginAviso").text("Opz! Nao deu certo. Tente novamente.");
                }
            },"json");
}